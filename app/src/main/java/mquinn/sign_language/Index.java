package mquinn.sign_language;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Index extends AppCompatActivity {

    private ViewPager nSlideViewPager;
    private LinearLayout nLayout;
    private SilderAdapter silderAdapter;

    private TextView[] nDots;
    Button nextbtn, skipbtn;
    Button startbtn;
    Animation btnAnim;

    private int currentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        nSlideViewPager = findViewById(R.id.screen_viewpager);
        nLayout = findViewById(R.id.layout);
        startbtn = findViewById(R.id.btn_getstarted);
        nextbtn = findViewById(R.id.next);
        btnAnim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.button_animation);
        skipbtn = findViewById(R.id.skip);

        silderAdapter = new SilderAdapter(this);

        nSlideViewPager.setAdapter(silderAdapter);

        addDotsIndicator(0);

        nSlideViewPager.addOnPageChangeListener(viewListener);

        nextbtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){

                nSlideViewPager.setCurrentItem(currentPage + 1);
            }
        });

        skipbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent home = new Intent(getApplicationContext(), Home.class);
                startActivity(home);
                finish();
            }
        });

        startbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent home = new Intent(getApplicationContext(), Home.class);
                startActivity(home);
                finish();
            }
        });

    }



    public void addDotsIndicator(int position){

        nDots =  new TextView[4];
        nLayout.removeAllViews();

        for(int i = 0; i < nDots.length; i++){

            nDots[i] = new TextView(this);
            nDots[i].setText(Html.fromHtml("&#8226;"));
            nDots[i].setTextSize(35);
            nDots[i].setTextColor(getResources().getColor(R.color.pred));

            nLayout.addView(nDots[i]);
        }

        if(nDots.length > 0){
            nDots[position].setTextColor(getResources().getColor(R.color.green));
        }
    }



    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {

            addDotsIndicator(i);
            currentPage = i;

            if( i == 0 ){
                nextbtn.setEnabled(true);
                startbtn.setEnabled(false);
                startbtn.setVisibility(View.INVISIBLE);

            }else if(i == nDots.length - 1) {
                nextbtn.setVisibility(View.INVISIBLE);
                startbtn.setVisibility(View.VISIBLE);
                skipbtn.setVisibility(View.INVISIBLE);

                startbtn.setAnimation(btnAnim);

            }

        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };


}
