package mquinn.sign_language;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SilderAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;

    public SilderAdapter(Context context){

        this.context = context;
    }

    public int[] slide_image ={
            R.drawable.tutorial_icon,
            R.drawable.add_icon,
            R.drawable.learn_icon,
            R.drawable.recongize_icon
    };

    public String[] slide_headings ={

            "Tutorial",
            "Create",
            "Dictionary",
            "Recognize"
    };

    public String[] slide_desc ={
            "There is some tutorial for you!\nLearn how to use this application.",
            "Select the gesture pictures form your phone.\nYou can put it into application.",
            "Enjoy the learning!\nDon't forget to try other functions.",
            "You can upload the pictures.\nLet system detect what gesture is it."
    };




    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == (RelativeLayout) o;
    }

    public Object instantiateItem(ViewGroup container, int position){

        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.layout_screen, container, false);

        ImageView slideImageView = (ImageView) view.findViewById(R.id.imageView);
        TextView sildeHeading = (TextView) view.findViewById(R.id.intro_title);
        TextView slideDescription = (TextView) view.findViewById(R.id.intro_desciption);

        slideImageView.setImageResource(slide_image[position]);
        sildeHeading.setText(slide_headings[position]);
        slideDescription.setText(slide_desc[position]);

        container.addView(view);

        return  view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        container.removeView((RelativeLayout)object);
    }
}
