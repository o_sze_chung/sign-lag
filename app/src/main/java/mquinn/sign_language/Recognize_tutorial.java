package mquinn.sign_language;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Recognize_tutorial extends AppCompatActivity {

    ImageView Back, Learn;
    Button Start, Save;

    BitmapDrawable drawable;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recognize_tutorial);

        Back = findViewById(R.id.arrow);
        Start = findViewById(R.id.start);
        Save = findViewById(R.id.download);
        Learn = findViewById(R.id.learing);

        Start.setOnClickListener(new View.OnClickListener()

        {
            public void onClick (View v){
                startActivity(new Intent(getApplicationContext(), Recognize.class));
            }
        });

        Back.setOnClickListener(new View.OnClickListener()

        {
            public void onClick (View v){
                startActivity(new Intent(getApplicationContext(), Home.class));
            }
        });

        Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawable = (BitmapDrawable)Learn.getDrawable();
                bitmap = drawable.getBitmap();

                FileOutputStream outputStream =null;

                File sdCard = Environment.getExternalStorageDirectory();
                File directory = new File(sdCard.getAbsolutePath() + "/YourFolderName");
                directory.mkdir();

                String fileName = String.format("%d.jpg", System.currentTimeMillis());
                File outFile = new File(directory,fileName);

                Toast.makeText(Recognize_tutorial.this, "Image saved Successfully", Toast.LENGTH_SHORT).show();

                try{
                    outputStream = new FileOutputStream(outFile);
                    bitmap.compress(Bitmap.CompressFormat.JPEG,100,outputStream);
                    outputStream.flush();
                    outputStream.close();

                    Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    intent.setData(Uri.fromFile(outFile));
                    sendBroadcast(intent);

                }catch (FileNotFoundException e){
                    e.printStackTrace();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });

    }
}
