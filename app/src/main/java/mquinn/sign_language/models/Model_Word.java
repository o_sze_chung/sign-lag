package mquinn.sign_language.models;

public class Model_Word {

    String uDescr, uEmail, uId, uImage, uTime, uTitle, uid;

    public Model_Word() {
    }

    public String getuId() {
        return uId;
    }

    public String getuImage() {
        return uImage;
    }

    public String getuDescr() {
        return uDescr;
    }

    public String getuEmail() {
        return uEmail;
    }

    public String getuTime() {
        return uTime;
    }

    public String getuTitle() {
        return uTitle;
    }

    public String getUid() {
        return uid;
    }

    public Model_Word(String uDescr, String uEmail, String uId, String uImage, String uTime, String uTitle, String uid) {
        this.uDescr = uDescr;
        this.uEmail = uEmail;
        this.uId = uId;
        this.uImage = uImage;
        this.uTime = uTime;
        this.uTitle = uTitle;
        this.uid = uid;
    }
}

