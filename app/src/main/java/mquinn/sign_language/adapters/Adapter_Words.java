package mquinn.sign_language.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import mquinn.sign_language.R;
import mquinn.sign_language.models.Model_Word;

public class Adapter_Words extends RecyclerView.Adapter<Adapter_Words.MyHolder>{

    Context context;
    List<Model_Word> wordList;

    public Adapter_Words(Context context, List<Model_Word> wordList) {
        this.context = context;
        this.wordList = wordList;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_words, viewGroup, false);

        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder myHolder, int i) {

        String uid = wordList.get(i).getUid();
        String uEmail = wordList.get(i).getuEmail();
        String uId = wordList.get(i).getuId();
        String uTitle = wordList.get(i).getuTitle();
        String uDescription = wordList.get(i).getuDescr();
        String uImage = wordList.get(i).getuImage();
        String uTimeStamp = wordList.get(i).getuTime();

        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTimeInMillis(Long.parseLong(uTimeStamp));
        String uTime = DateFormat.format("dd/MM/yyyy hh:mm aa", calendar).toString();

        myHolder.pTimeTv.setText(uTime);
        myHolder.pTitleTv.setText(uTitle);
        myHolder.pDescriptionTv.setText(uDescription);

        if(uImage.equals("noImage")){
            myHolder.pImageIv.setVisibility(View.GONE);
        }else {

            try {
                Picasso.get().load(uImage).into(myHolder.pImageIv);
            } catch (Exception e) {

            }
        }

        myHolder.moreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "More", Toast.LENGTH_SHORT).show();
            }
        });

        myHolder.shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "Share", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return wordList.size();
    }

    class MyHolder extends RecyclerView.ViewHolder{

        LinearLayout card;
        ImageView pImageIv;
        TextView uNameTv, pTimeTv, pTitleTv, pDescriptionTv;
        ImageButton moreBtn;
        Button favoriteBtn, shareBtn;

        public MyHolder(@NonNull View itemView) {
            super(itemView);

            card = itemView.findViewById(R.id.card);
           pImageIv = itemView.findViewById(R.id.pImageIv);
            pTimeTv = itemView.findViewById(R.id.pTimeTv);
            pTitleTv = itemView.findViewById(R.id.pTitleTv);
            pDescriptionTv = itemView.findViewById(R.id.pDescriptionTv);
            shareBtn = itemView.findViewById(R.id.shareBtn);
            moreBtn = itemView.findViewById(R.id.moreBtn);

        }
    }
}
