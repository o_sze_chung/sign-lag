package mquinn.sign_language;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class Register extends AppCompatActivity {

    EditText Name, Email, Password, CPassword;
    Button RegisterBtn;
    TextView LoginBtn;
    FirebaseAuth fAuth;
    ImageView Back;
    FirebaseUser user;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Name = findViewById(R.id.name);
        Email = findViewById(R.id.mail);
        Password = findViewById(R.id.password);
        CPassword = findViewById(R.id.cpassword);
        LoginBtn = findViewById(R.id.Login);
        RegisterBtn = findViewById(R.id.registerBtn);
        Back = findViewById(R.id.backarrow);

        fAuth = FirebaseAuth.getInstance();

    RegisterBtn.setOnClickListener(new View.OnClickListener()

    {
        @Override
        public void onClick (View v){
        String email = Email.getText().toString().trim();
        String password = Password.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Email.setError("Email is Required.");
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Password.setError("Password is Required.");
            return;
        }

        if (password.length() < 6) {
            Password.setError("Password Must be >= 6 Characters");
        }

        fAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    FirebaseUser user = fAuth.getCurrentUser();
                    if (task.getResult().getAdditionalUserInfo().isNewUser()) {
                        String email = user.getEmail();
                        String uid = user.getUid();

                        HashMap<Object, String> hasMap = new HashMap<>();

                        hasMap.put("email", email);
                        hasMap.put("uid", uid);
                        hasMap.put("name", "");
                        hasMap.put("image", "");

                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference reference = database.getReference("Users");
                        reference.child(uid).setValue(hasMap);
                    }
                    Toast.makeText(Register.this, "User Created.", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), Index.class));
                } else {
                    Toast.makeText(Register.this, "Error!" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    });

        LoginBtn.setOnClickListener(new View.OnClickListener()

    {
        public void onClick (View v){
        startActivity(new Intent(getApplicationContext(), Login.class));
    }
    });

        Back.setOnClickListener(new View.OnClickListener()

    {
        public void onClick (View v){
        startActivity(new Intent(getApplicationContext(), Welcome.class));
    }
    });
}
}
