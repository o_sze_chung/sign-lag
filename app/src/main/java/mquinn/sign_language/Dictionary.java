package mquinn.sign_language;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import mquinn.sign_language.adapters.Adapter_Words;
import mquinn.sign_language.models.Model_Word;

public class Dictionary extends AppCompatActivity {

    FirebaseAuth firebaseAuth;

    RecyclerView recyclerView;
    List<Model_Word> postList;
    Adapter_Words adapterPosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);

        firebaseAuth = FirebaseAuth.getInstance();

        recyclerView = findViewById(R.id.dictionaryview);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        postList = new ArrayList<Model_Word>();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Words");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for( DataSnapshot ds:dataSnapshot.getChildren()) {
                    Model_Word modelPost = ds.getValue(Model_Word.class);
                    postList.add(modelPost);

                }

                    adapterPosts = new Adapter_Words(Dictionary.this, postList);
                recyclerView.setAdapter(adapterPosts);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(Dictionary.this, ""+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
//
//    private void searchPosts(String searchQuery){}
}

