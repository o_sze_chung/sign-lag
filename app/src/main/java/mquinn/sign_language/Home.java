package mquinn.sign_language;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;

public class Home extends AppCompatActivity {

    LinearLayout Create,logout, Profile, Recognize, Dictionary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Create = findViewById(R.id.create);
        logout = findViewById(R.id.logout);
        Profile = findViewById(R.id.profile);
        Recognize = findViewById(R.id.recognize);
        Dictionary = findViewById(R.id.dictionary);

        Recognize.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                startActivity(new Intent(getApplicationContext(),Recognize_tutorial.class));
            }
        });

        Create.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                startActivity(new Intent(getApplicationContext(),Create.class));
            }
        });

        Dictionary.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                startActivity(new Intent(getApplicationContext(),Dictionary.class));
            }
        });

//        Profile.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                startActivity(new Intent(getApplicationContext(),Profile.class));
//            }
//        });
//
}
    public void logout(View view){
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(getApplicationContext(), Login.class));
        finish();
    }

}
